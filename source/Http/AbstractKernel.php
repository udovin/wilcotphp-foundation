<?php namespace Wilcot\Foundation\Http;

use Wilcot\Foundation\AbstractKernel as BaseKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 *
 * @since 0.1.0
 */
abstract class AbstractKernel extends BaseKernel
{
	/**
	 *
	 *
	 * @param Request $request
	 * @return Response
	 */
	abstract public function handle(Request $request);
};
