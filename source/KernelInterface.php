<?php namespace Wilcot\Foundation;

/**
 *
 *
 * @since 0.1.0
 */
interface KernelInterface
{
	/**
	 *
	 *
	 * @return ApplicationInterface
	 */
	public function getApplication();
};
