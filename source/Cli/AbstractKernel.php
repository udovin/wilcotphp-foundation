<?php namespace Wilcot\Foundation\Cli;

use Wilcot\Foundation\AbstractKernel as BaseKernel;

/**
 *
 *
 * @since 0.1.0
 */
abstract class AbstractKernel extends BaseKernel
{
	/**
	 *
	 *
	 * @return int
	 */
	abstract public function handle();
};
