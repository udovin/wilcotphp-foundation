<?php namespace Wilcot\Foundation;

/**
 *
 *
 * @since 0.1.0
 */
interface ApplicationInterface
{
	/**
	 * Get service
	 *
	 * @param string $name
	 * @return mixed
	 */
	public function getService($name);

	/**
	 * Set service
	 *
	 * @param string $name
	 * @param mixed $service
	 * @return this
	 */
	public function setService($name, $service);

	/**
	 * Check that the service exists
	 *
	 * @param string $name
	 * @return bool
	 */
	public function hasService($name);
};
