<?php namespace Wilcot\Foundation;

/**
 *
 *
 * @since 0.1.0
 */
abstract class AbstractKernel implements KernelInterface
{
	/**
	 * @var ApplicationInterface $application
	 */
	private $application;

	/**
	 * A constructor
	 */
	public function __construct(ApplicationInterface $application)
	{
		$this->application = $application;
	}

	/**
	 *
	 *
	 * @return ApplicationInterface
	 */
	public function getApplication()
	{
		return $this->application;
	}
};
