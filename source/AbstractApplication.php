<?php namespace Wilcot\Foundation;

/**
 *
 *
 * @since 0.1.0
 */
abstract class AbstractApplication implements ApplicationInterface
{
	/**
	 * @var array $services
	 */
	private $services;

	/**
	 * A constructor
	 */
	public function __construct()
	{
		$this->services = array();
	}

	/**
	 * Get service
	 *
	 * @param string $name
	 * @return mixed
	 */
	public function getService($name)
	{
		return $this->services[$name];
	}

	/**
	 * Set service
	 *
	 * @param string $name
	 * @param mixed $service
	 * @return this
	 */
	public function setService($name, $service)
	{
		$this->services[$name] = $service;

		return $this;
	}

	/**
	 * Check that the service exists
	 *
	 * @param string $name
	 * @return bool
	 */
	public function hasService($name)
	{
		return array_key_exists($name, $this->services);
	}
};
